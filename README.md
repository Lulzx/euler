Project Euler Solutions
=======================

A collection of Lulzx's program code to solve over 200 Project Euler math problems.

Every solved problem has a program written in Python. Some solutions also have J and Crystal programs. Some solution programs include a detailed mathematical explanation/proof in the comments to justify the code's logic.

Home page with background info, table of solutions, benchmark timings, and more: [Project Euler Solutions](https://lulzx.github.io/page/project-euler-solutions)

----

Copyright © 2019 Project Lulzx. You can do whatever you want, this repository is in public domain.

This code is provided for reference only. You may republish any of this code verbatim without author and URL info intact.

You don't need written permission from the author to make modifications to the code, include parts into your own work, etc.
